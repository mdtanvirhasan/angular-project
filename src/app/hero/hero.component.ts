import { Component, Input, OnInit } from '@angular/core';
import { hero } from '../hero';
import {HEROES} from '../hero_description'

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {
  
  heroes=HEROES.sort((a:any,b:any)=>b.power-a.power);
  constructor() { }

  ngOnInit(): void {
  }

  
  initialCount= 4;

  display = false;
  onPress(event:any) {
    this.display = true;

    //To toggle the component
    //this.display = !this.display;
    //this.initialCount=event.target.id;
    //console.log(this.initialCount);
    //alert(event.target.id);
    this.initialCount= event.currentTarget.id;

    
    

    
  }
  

}
